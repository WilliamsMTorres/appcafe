/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Uti;

/**
 *
 * @author MustainE
 */
public class Utilidades {

    public static final int idempresa = 0;
    public static final int idcp = 1;
    public static final int idcafepractice = 2;
    public static final int codagri = 3;
    public static final int apeagri = 4;
    public static final int nomagri = 5;
    public static final int dniagri = 6;
    public static final int nomfinca = 7;
    public static final int nomanexo = 8;
    public static final int cpinicial = 9;
    public static final int orginicial = 10;
    public static final int ftinicial = 11;
    public static final int rainfinicial = 12;
    public static final int convinicial = 13;
    public static final int cpavance = 14;
    public static final int orgavance = 15;
    public static final int ftavance = 16;
    public static final int rainfavance = 17;
    public static final int convavance = 18;
    public static final int cpdisponible = 19;
    public static final int orgdisponible = 20;
    public static final int ftdisponible = 21;
    public static final int rainfdisponible = 22;
    public static final int convdisponible = 23;   
    public static int filaSeleccionada;
}
